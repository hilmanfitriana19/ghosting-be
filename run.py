#!env/bin/python
import os
from src.app import create_app

app_mode = 'development'
app = create_app(app_mode)

if __name__=='__main__':
    app.run(host='0.0.0.0',port='5001',debug=True)