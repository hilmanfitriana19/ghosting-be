from marshmallow import fields, Schema
from src.models.CartProductModel import CartProductScheme, CartProductModel, CartProductDetailScheme
from . import db

class CartModel(db.Model):

    # table name
    __tablename__ = 'cart'
    cart_id = db.Column(db.String(50),primary_key=True)
    user_id = db.Column(db.String(50), db.ForeignKey('user.user_id')) 
    total_payment = db.Column(db.Integer, nullable=False, default=0)
    cart_product = db.relationship("CartProductModel", backref="parent")     

    # class constructor
    def __init__(self, data):                        
        self.cart_id = 'CART'+str(CartModel.get_count_cart())
        self.user_id = data.get('user_id')
        self.total_payment = data.get('total_payment')

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():            
            setattr(self, key, item)        
        db.session.commit()        

    def delete(self):
        db.session.delete(self)
        db.session.commit()        

    def set_total(self,total):
        self.total_payment=total;
        db.session.commit()

    @staticmethod
    def get_count_cart():
        return db.session.query(CartModel.user_id).count() 

    @staticmethod
    def get_cart_by_user_id(user):
        return CartModel.query.filter_by(user_id=user).first()

    @staticmethod
    def getUserCart(user):
        return CartModel.query.filter_by(user_id=user).join(CartProductModel,CartProductModel.cart_id==CartModel.cart_id,isouter=True)

class CartScheme(Schema):
    cart_id = fields.Str(dump_only=True)
    user_id = fields.Str(required=True)
    total_payment = fields.Int(required=True)
    cart_product = fields.Nested(CartProductScheme,many=True,exclude=("cart_id",))

class CartDetailProductScheme(Schema):        
    total_payment = fields.Int(required=True)
    cart_product = fields.Nested(CartProductDetailScheme,many=True)