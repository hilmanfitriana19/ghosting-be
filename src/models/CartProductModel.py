from marshmallow import fields, Schema
from . import db
from src.models.VariationModel import VariationModel
from src.models.ProductModel import ProductModel
from src.models.DetailProductModel import DetailProductModel

class CartProductModel(db.Model):

    # table name
    __tablename__ = 'cart_product'
    __table_args__ = (
        db.UniqueConstraint('cart_id', 'product_id', 'variation_id', 'detail_product_id', name='cart_id'),        
        db.UniqueConstraint('cart_product_id', name='cart_product_id'),
    )
    cart_product_id = db.Column(db.String(50),primary_key=True)    
    cart_id = db.Column(db.String(50),db.ForeignKey('cart.cart_id'))    
    product_id = db.Column(db.String(15), db.ForeignKey('product.product_id'))
    variation_id = db.Column(db.String(50),db.ForeignKey('variation.variation_id'))
    detail_product_id = db.Column(db.String(50),db.ForeignKey('detail_product.detail_product_id'))
    quantity = db.Column(db.Integer, nullable=False)

    detail_product = db.relationship("DetailProductModel",uselist=False,lazy='select')    
    product = db.relationship("ProductModel",uselist=False,lazy='select')    
    variation = db.relationship("VariationModel",uselist=False,lazy='select')        

    # class constructor
    def __init__(self, data):                        
        self.cart_product_id = data.get('cart_id')+str(CartProductModel.get_count_of_cart(data.get('cart_id')))
        self.cart_id = data.get('cart_id')
        self.variation_id = data.get('variation_id')
        self.detail_product_id = data.get('detail_product_id')
        self.product_id = data.get('product_id')
        self.quantity = data.get('quantity')        

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():            
            setattr(self, key, item)       
        db.session.commit()        

    @staticmethod
    def get_count_of_cart(cart_id):
        return CartProductModel.query.filter_by(cart_id=cart_id).count()+1

    @staticmethod  
    def get_card_by_id(id):
        return CartProductModel.query.filter_by(cart_product_id=id).first()
    
    @staticmethod  
    def get_card_by_detail_product_id(id):
        return CartProductModel.query.filter_by(detail_product_id=id).first()

    @staticmethod
    def get_card_product_by_cart_product_detailpro(cart,product,detail_pro):
        return CartProductModel.query.filter_by(cart_id=cart,product_id=product,detail_product_id=detail_pro).first()
    
    def add_quantity(self,quantity):        
        self.quantity=self.quantity+quantity
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()    

class CartProductScheme(Schema):
    cart_product_id = fields.Str(dump_only=True)
    cart_id = fields.Str(required=True)
    product = fields.Function(lambda obj:obj.product.name)
    variation = fields.Function(lambda obj:obj.variation.variation_name)
    detail_product = fields.Function(lambda obj:obj.detail_product.detail_product_name)
    quantity = fields.Integer(required=True)
    price = fields.Function(lambda obj:obj.product.price)

class CartProductSchemeAdd(Schema):
    cart_product_id = fields.Str(dump_only=True)
    cart_id = fields.Str(required=True)
    product_id = fields.Str(required=True)
    variation_id = fields.Str(required=True)
    detail_product_id = fields.Str(required=True)
    quantity = fields.Integer(required=True)    

class CartProductDetailScheme(Schema):
    cart_product_id = fields.Str(dump_only=True)
    cart_id = fields.Str(required=True)    
    detail_product_id = fields.Str(required=True)
    quantity = fields.Integer(required=True)    
    product_id = fields.Str(required=True)