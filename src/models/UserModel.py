from marshmallow import fields, Schema
from src.models.UserAddressModel import UserAddressModel
import datetime

from . import db, bcrypt
class UserModel(db.Model):

    # table name
    __tablename__ = 'user'
    user_id = db.Column(db.String(25), primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String(200), nullable=False)
    email = db.Column(db.String(100), nullable=False, unique=True)
    createdAt = db.Column(db.DateTime(),default=datetime.datetime.now())
    modifiedAt = db.Column(db.DateTime())
    email_confirmation_sent_on = db.Column(db.DateTime(), nullable=True)
    email_confirmed = db.Column(db.Boolean, nullable=True, default=False)
    email_confirmed_on = db.Column(db.DateTime(), nullable=True)
    phone = db.Column(db.String(20),nullable=False)
    address = db.relationship("UserAddressModel", backref="parent")
    is_sa = db.Column(db.Integer,default=False)
    

    # class constructor
    def __init__(self, data):
        self.username = data.get('username')
        self.password = self.__generate_hash(data.get('password'))
        self.createdAt = datetime.datetime.utcnow()
        self.modifiedAt = datetime.datetime.utcnow()
        self.phone = data.get('phone')
        self.email = data.get('email')        
        self.email_confirmation_sent_on=datetime.datetime.utcnow()
        self.email_confirmed_on = None
        self.user_id = 'USR'+str(UserModel.get_count_user())

    @staticmethod
    def get_count_user():
        return db.session.query(UserModel.user_id).count()        

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():
            if key == "password" and item != self.password and item !=None and item!='':                
                temp = self.__generate_hash(item)                
                setattr(self, key, temp)
            else:
                setattr(self, key, item)
        self.modifiedAt = datetime.datetime.utcnow()
        db.session.commit()    

    def check_lifetime_token(self):        
        if (datetime.datetime.utcnow() - self.email_confirmation_sent_on).seconds < 3600:
            return True
        else:
            return False

    def verify_user_mail(self):
        self.email_confirmed_on=datetime.datetime.utcnow()
        self.email_confirmed=True
        db.session.commit()

    def update_pass(self,password):        
        temp=self.__generate_hash(password)
        self.password=temp
        db.session.commit()

    def email_confirmed(self):
        return self.email_confirmed

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def __generate_hash(self, password):
        return bcrypt.generate_password_hash(password, rounds=13).decode("utf-8")

    def check_hash(self, password):
        return bcrypt.check_password_hash(self.password, password)    
    
    @staticmethod
    def get_users():        
        return UserModel.query.order_by(UserModel.id).all()
    
    @staticmethod
    def get_user(id):
        return UserModel.query.filter_by(user_id=id).first()
    
    @staticmethod
    def get_user_by_username(username):
        return UserModel.query.filter_by(username=username).first()
    
    @staticmethod
    def get_user_by_email(email):
        return UserModel.query.filter_by(email=email).first()
    
    @staticmethod
    def get_user_by_phone(phone):
        return UserModel.query.filter_by(phone=phone).first()

class UserScheme(Schema):
    username = fields.Str(required=True)
    password = fields.Str(required=True)            
    email = fields.Str(required=True)    
    phone = fields.Str(required=True)

class UserLoginScheme(Schema):    
    password = fields.Str(required=True)
    username = fields.Str(required=True)