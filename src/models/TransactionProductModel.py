from marshmallow import fields, Schema
from . import db
from src.models.DetailProductModel import DetailProductModel

class TransactionProductModel(db.Model):

    # table name
    __tablename__ = 'transaction_product'    
    
    transaction_product_id = db.Column(db.String(50),primary_key=True)    
    transaction_id = db.Column(db.String(50),db.ForeignKey('transaction.transaction_id'))
    product_id = db.Column(db.String(15), db.ForeignKey('product.product_id'))
    detail_product_id = db.Column(db.String(50),db.ForeignKey('detail_product.detail_product_id'))
    quantity = db.Column(db.Integer, nullable=False)

    detail_product = db.relationship("DetailProductModel",uselist=False,lazy='select')    

    # class constructor
    def __init__(self, data):                        
        self.transaction_product_id = data.get('transaction_id')+str(TransactionProductModel.get_count_transaction(data.get('transaction_id')))
        self.transaction_id = data.get('transaction_id')        
        self.detail_product_id = data.get('detail_product_id')
        self.product_id = data.get('product_id')
        self.quantity = data.get('quantity')        

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():            
            setattr(self, key, item)        
        db.session.commit()        

    @staticmethod
    def get_count_transaction(transaction_id):
        return TransactionProductModel.query.filter_by(transaction_id=transaction_id).count()

    @staticmethod  
    def get_transaction_by_id(id):
        return TransactionProductModel.query.filter_by(transaction_product_id=id).first()    
    
    @staticmethod  
    def get_transaction_by_detail_product_id(id):
        return TransactionProductModel.query.filter_by(detail_product_id=id).first()
        
    def delete(self):
        db.session.delete(self)
        db.session.commit()    

class TransactionProductScheme(Schema):
    transaction_product_id = fields.String(dump_only=True)
    transaction_id = fields.Str(required=True)    
    product_id = fields.Str(required=True)
    detail_product_id = fields.Str(required=True)
    detail_product = fields.Function(lambda obj:obj.detail_product.detail_product_name)
    quantity = fields.Integer(required=True)    