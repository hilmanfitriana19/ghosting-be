from marshmallow import fields, Schema
from . import db
from src.utils.ProductUtils import *
from src.models.DetailProductModel import DetailProductScheme, DetailProductScheme

class VariationModel(db.Model):

    # table name
    __tablename__ = 'variation'
    __table_args__ = (
        db.UniqueConstraint('variation_id', 'product_id'),
    )
        
    variation_id = db.Column(db.String(50), primary_key=True)
    product_id = db.Column(db.String(25),db.ForeignKey('product.product_id'),nullable=False)
    variation_name = db.Column(db.String(50),nullable=False)
    detail_product = db.relationship("DetailProductModel", backref="parent")    

    # class constructor
    def __init__(self, data):
        if data.get('variation_id'):
            self.variation_id = data.get('variation_id')
        else:
            self.variation_id = ('VR'+data.get('product_id')+remove_vocal(data.get('variation_name')).replace(' ','_')+str(VariationModel.get_count_variant_product(data.get('product_id')))).upper()
        self.product_id = data.get('product_id')
        self.variation_name = data.get('variation_name')        

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():            
            setattr(self, key, item)        
        db.session.commit()        
    
    def update_stock(self,value):
        self.stock = value
        db.session.commit()
    
    def delete(self):
        db.session.delete(self)
        db.session.commit()    

    @staticmethod
    def get_count_variant_product(product_id):        
        return VariationModel.query.filter_by(product_id=product_id).count()
    
    @staticmethod
    def get_variant_by_id_product(id,product_id):        
        return VariationModel.query.filter_by(variation_id=id,product_id=product_id).all()

    @staticmethod 
    def get_variation_from_id(id):
        return VariationModel.query.filter_by(variation_id=id).first()

class VariationScheme(Schema):
    variation_id = fields.Str(required=True)
    product_id = fields.Str(required=True)
    variation_name = fields.Str(required=True)

class VariationSchemeFull(Schema):
    variation_id = fields.Str(dump_only=True)    
    variation_name = fields.Str(required=True)    
    detail_product = fields.Nested(DetailProductScheme,many=True,exclude=("variation_id",))