from marshmallow import fields, Schema
from src.models.ProductModel import ProductScheme, ProductSchemeFull

from . import db

class CategoryModel(db.Model):

    # table name
    __tablename__ = 'category'
    category_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(20), unique=True, nullable=False)    
    product = db.relationship("ProductModel", backref="parent")

    # class constructor
    def __init__(self, data):
        self.category_id = data.get('category_id')
        self.name = data.get('name')        

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():            
            setattr(self, key, item)        
        db.session.commit()        

    def delete(self):
        db.session.delete(self)
        db.session.commit()    
    
    @staticmethod
    def get_category():
        return CategoryModel.query.order_by(CategoryModel.category_id).all()        
    
    @staticmethod
    def get_category_full():
        return CategoryModel.query.order_by(CategoryModel.category_id).all()        
    
    @staticmethod
    def get_category_by_id(id):
        return CategoryModel.query.filter_by(category_id=id).first()

class CategoryScheme(Schema):
    category_id = fields.Integer(dump_only=True)
    name = fields.Str(required=True)

class CategoryProductScheme(Schema):
    category_id = fields.Integer(dump_only=True)
    name = fields.Str(required=True)
    product = fields.Nested(ProductScheme,many=True,exclude=('category',))