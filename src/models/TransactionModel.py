from marshmallow import fields, Schema
from src.models.DetailProductModel import DetailProductModel, DetailProductScheme
from src.models.UserModel import UserModel, UserScheme
from src.models.ProductModel import ProductScheme, ProductModel
from src.models.TransactionProductModel import TransactionProductModel, TransactionProductScheme
import datetime

from . import db

class TransactionModel(db.Model):

    # table name
    __tablename__ = 'transaction'
    transaction_id = db.Column(db.String(50),primary_key=True)        
    date = db.Column(db.DateTime())
    total_payment = db.Column(db.Integer(),nullable=False)    
    user_address_id = db.Column(db.String(50), db.ForeignKey('user_address.address_id'))

    transaction_product = db.relationship("TransactionProductModel", backref="parent")    

    # class constructor
    def __init__(self, data):  
        self.user_id = data.get('user_id')
        self.date= datetime.datetime.now()
        self.transaction_id = 'TRS'+str(TransactionModel.get_count())
        self.total_payment = data.get('total_payment')        
        self.user_address_id = data.get('user_address_id')        

    @staticmethod
    def get_count():
        return db.session.query(TransactionModel.transaction_id).count()
    
    @staticmethod
    def get_transaction_by_user(user_id):
        return TransactionModel.query.filter(TransactionModel.user_address_id.like(user_id+"%")).all()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():            
            setattr(self, key, item)        
        db.session.commit()        

    def delete(self):
        db.session.delete(self)
        db.session.commit()    


class TransactionScheme(Schema):        
    transaction_id = fields.Str(required=True)    
    user_address_id = fields.Str(required=True)    
    total_payment = fields.Integer()    
    date = fields.DateTime()
    transaction_product = fields.Nested(TransactionProductScheme,many=True,exclude=("transaction_product_id",))