from marshmallow import fields, Schema
from . import db

class UserAddressModel(db.Model):

    # table name
    __tablename__ = 'user_address'
    address_id = db.Column(db.String(50),primary_key=True)
    addressee = db.Column(db.String(30),nullable=False)
    country = db.Column(db.String(40),nullable=False)
    phone = db.Column(db.String(20),nullable=False)    
    region = db.Column(db.String(50), nullable=False)
    district = db.Column(db.String(255), nullable=False)
    street_name = db.Column(db.String(40), nullable=False)
    postal_code = db.Column(db.String(10), nullable=False)
    other = db.Column(db.String(40))
    user_id = db.Column(db.String(50),db.ForeignKey('user.user_id'))
    user = db.relationship("UserModel",uselist=False,lazy='select')    

    # class constructor
    def __init__(self, data):        
        self.address_id = data.get('user_id')+'_ADDR'+str(UserAddressModel.get_address_count(data.get('user_id')))
        self.user_id = data.get('user_id')
        self.addressee = data.get('addressee')
        self.phone = data.get('phone')
        self.country = data.get('country')
        self.region = data.get('region')
        self.postal_code = data.get('postal_code')
        self.district = data.get('district')
        self.street_name = data.get('street_name')
        self.other = data.get('other')        

    @staticmethod
    def get_address_count(user_id):        
        return UserAddressModel.query.filter_by(user_id=user_id).count()
    
    @staticmethod
    def get_user_address(user_id):
        return UserAddressModel.query.filter_by(user_id=user_id).all()
    
    @staticmethod
    def get_user_address_by_address_id(id):
        return UserAddressModel.query.filter_by(address_id=id).first()

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():            
            setattr(self, key, item)        
        db.session.commit()        

    def delete(self):
        db.session.delete(self)
        db.session.commit()    

class UserAddressScheme(Schema):
    address_id = fields.Str(dump_only=True)
    addressee = fields.Str(required=True)
    phone = fields.Str(required=True)            
    country = fields.Str(required=True)
    region = fields.Str(required=True)    
    district = fields.Str(required=True)
    postal_code = fields.Str(required=True)
    street_name = fields.Str(required=True)    
    other = fields.Str()