from marshmallow import fields, Schema
from . import db

class PaymentModel():
    card_number = None    
    total_payment = 0        
    name = None
    method = None
    cvc = None
    expiration_month = None
    expiration_year = None

    def __init__(self, data):
        self.card_number = data.get('card_number')
        self.method = data.get('method')
        self.total_payment = data.get('total_payment')                
        self.name = data.get('name')
        self.cvc = data.get('cvc')
        self.expiration_month = data.get('expiration_month')
        self.expiration_year = data.get('expiration_year')


class PaymentScheme(Schema):
    card_number = fields.Str(required=True)
    method = fields.Str(required=True)
    total_payment = fields.Int(required=True)
    cvc = fields.Str(required=True)
    name = fields.Str(required=True)
    expiration_month = fields.Str(required=True)
    expiration_year = fields.Str(required=True)
