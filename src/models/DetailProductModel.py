from marshmallow import fields, Schema
from . import db
from src.utils.ProductUtils import *

class DetailProductModel(db.Model):

    # table name
    __tablename__ = 'detail_product'
        
    detail_product_id = db.Column(db.String(50), primary_key=True)
    variation_id = db.Column(db.String(50),db.ForeignKey('variation.variation_id'),nullable=False)
    detail_product_name = db.Column(db.String(50),nullable=False)
    stock = db.Column(db.Integer,nullable=False)    

    # class constructor
    def __init__(self, data):        
        if data.get('detail_product_id'):
            self.detail_product_id = data.get('detail_product_id')
        else:
            self.detail_product_id = ('DR'+remove_vocal(data.get('variation_id')+str(DetailProductModel.get_count_of_variant(data.get('variation_id'))))).upper()
        self.variation_id = data.get('variation_id')
        self.detail_product_name = data.get('detail_product_name')
        self.stock = data.get('stock')        

    def save(self):
        db.session.add(self)
        db.session.commit()

    def update(self, data):
        for key, item in data.items():            
            setattr(self, key, item)        
        db.session.commit()
    
    def update_stock(self,value):
        self.stock = value
        db.session.commit()

    @staticmethod
    def get_count_of_variant(variant):
        return DetailProductModel.query.filter_by(variation_id=variant).count()

    @staticmethod
    def get_detail_by_id(id):
        return DetailProductModel.query.filter_by(detail_product_id=id).first()

    @staticmethod    
    def get_by_variation_and_detail_product(variant,detail_product):                
        return DetailProductModel.query.filter_by(variation_id=variant,detail_product_id=detail_product).first()

    def delete(self):
        db.session.delete(self)
        db.session.commit()                

class DetailProductScheme(Schema):
    detail_product_id = fields.Str(required=True)
    variation_id = fields.Str(required=True)
    detail_product_name = fields.Str(required=True)
    stock = fields.Integer(required=True)
