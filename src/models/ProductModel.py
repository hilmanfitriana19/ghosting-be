from marshmallow import fields, Schema
from src.utils.ProductUtils import *
from src.models.VariationModel import VariationModel, VariationScheme, VariationSchemeFull

from . import db

class ProductModel(db.Model):

    # table name
    __tablename__ = 'product'
    product_id = db.Column(db.String(15), primary_key=True)
    name = db.Column(db.String(20), unique=True, nullable=False)
    description = db.Column(db.String(255), nullable=False)
    category = db.Column(db.Integer, db.ForeignKey('category.category_id'),nullable=False)
    price = db.Column(db.Integer,nullable=False)
    variation = db.relationship("VariationModel", backref="parent")
    category_obj = db.relationship("CategoryModel",uselist=False,lazy='select')    

    # class constructor
    def __init__(self, data):
        if data.get('product_id'):
            self.product_id = data.get('product_id')
        else:
            self.product_id = ('PR'+str(remove_vocal(data.get('name').replace(' ','_')[:4])+str(ProductModel.get_count_of_product()))).upper()
        self.name = data.get('name')
        self.description = data.get('description')
        self.category = data.get('category')        
        self.price = data.get('price')        
        

    def save(self):
        db.session.add(self)
        db.session.commit()
    
    def update(self, data):
        for key, item in data.items():            
            setattr(self, key, item)        
        db.session.commit()        

    def delete(self):
        db.session.delete(self)
        db.session.commit()    
    
    @staticmethod
    def get_count_of_product():        
        return db.session.query(ProductModel.product_id).count()

    @staticmethod
    def get_price(self):
        return self.price

    @staticmethod
    def get_product():
        return ProductModel.query.order_by(ProductModel.product_id).all()
    
    @staticmethod
    def get_product_by_id(id):
        return ProductModel.query.filter_by(product_id=id).first()
    
    @staticmethod
    def get_all_product_filter_by_category(category):                
        return ProductModel.query.filter_by(category = category).all()
    
    @staticmethod
    def get_product_full_detail_by_id(id):
        return ProductModel.query.filter_by(product_id=id).first()        

class ProductScheme(Schema):
    product_id = fields.Str(dump_only=True)
    name = fields.Str(required=True)
    description = fields.Str(required=True)
    category = fields.Integer(required=True)
    price = fields.Integer(required = True)
    stock = fields.Integer()
    img_url = fields.Str()

class ProductSchemeFull(Schema):
    product_id = fields.Str(dump_only=True)
    name = fields.Str(required=True)
    description = fields.Str(required=True)
    category = fields.Function(lambda obj:obj.category_obj.name)
    price = fields.Integer(required = True)
    variation = fields.Nested(VariationSchemeFull,many=True)