class MessageOperation():
    # successfull
    SUCCESFULL_CREATED = "Successfully Created"
    SUCCESFULL_UPDATED = "Successfully Updated"
    SUCCESFULL_DELETED = "Successfully Deleted"
    SUCCESFULL_GET = "Successfully Get Data"
    SUCCESFULL_LOGIN = "Successfully Login"
    SUCCESFULL_LOGOUT = "Successfully Logout"

    # unsuccessfull
    UNSUCCESFULL_CREATED = "Failed To Create Data"
    UNSUCCESFULL_UPDATED = "Failed To Update Data"
    UNSUCCESFULL_DELETED = "Failed To Delete Data"
    UNSUCCESFULL_GET = "Failed To Get Data"
    UNSUCCESFULL_LOGIN = "Failed to Login"
    UNSUCCESFULL_LOGOUT = "Failed to Logout"
    FAILED_OPERATION = "You Don't Have Permission"
    
class MessageDB():    
    ERROR_INTEGRITY = "Database Has Integrity Error"
    ERROR_NOTFOUND = "Data Not Found"
    ERROR_EMPTY = "Data Is Empty"
    ERROR_EXIST = "Data Already Exist"
    ERROR_ARGUMENT = "Database Has Detected Argument Error"

class UrlAPI():    
    AUTH_API = "/api/auth/"
    CART_API = "/api/cart/"
    CATEGORY_API = "/api/category/"
    PRODUCT_API = "/api/product/"
    TRANSACTION_API="/api/transaction/"
    USER_ADDRESS_API="/api/user/address/"

class AppJwt():
    SECRET_KEY = "JAM_IMPORT"

class EmailCons():
    USERNAME = 'hilman191025@gmail.com'
    PASSWORD = 'hilman123'
    DEFAULT_SENDER = 'hilman191025@gmail.com'
    SERVER = 'smtp.googlemail.com'
    PORT = 465
    SECRET_KEY = 'JamImport'
    SECURITY_PASSWORD_SALT = "JamImportSalt"

class LimiterAPI():
    AUTH_LIMITER = "3/5minute"
    TOKEN_LIMITER = "10/10minute"

class Server():
    DOMAIN='0.0.0.0:5001'

class FrontEnd():
    PATH = '../Fe-2/jam_import/'

class User():
    Name = 'hilman'