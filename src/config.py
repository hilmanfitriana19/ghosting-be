# /src/config.py
from src.constant import *

class Development(object):
    """
    Development environment configuration
    """
    DEBUG = True
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://jam_import:jam_import123@localhost:3306/jam_import'
    SECRET_KEY = EmailCons.SECRET_KEY
    SECURITY_PASSWORD_SALT = EmailCons.SECURITY_PASSWORD_SALT
    BCRYPT_LOG_ROUNDS = 13
    WTF_CSRF_ENABLED = True
    DEBUG_TB_ENABLED = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    
    MAIL_SERVER = EmailCons.SERVER
    MAIL_PORT = EmailCons.PORT
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True

    # gmail authentication
    MAIL_USERNAME = EmailCons.USERNAME
    MAIL_PASSWORD = EmailCons.PASSWORD

    # mail accounts
    MAIL_DEFAULT_SENDER = EmailCons.DEFAULT_SENDER

class Production(object):
    """
    Production environment configurations
    """
    DEBUG = False
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS=False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://jam_import:jam_import123@localhost:3306/jam_import'
    SECRET_KEY = EmailCons.SECRET_KEY
    SECURITY_PASSWORD_SALT = EmailCons.SECURITY_PASSWORD_SALT
    BCRYPT_LOG_ROUNDS = 13
    WTF_CSRF_ENABLED = True,
    DEBUG_TB_ENABLED = False,
    DEBUG_TB_INTERCEPT_REDIRECTS = False,
    
    MAIL_SERVER = EmailCons.SERVER
    MAIL_PORT = EmailCons.PORT,
    MAIL_USE_TLS = False,
    MAIL_USE_SSL = True,

    # gmail authentication
    MAIL_USERNAME = EmailCons.USERNAME,
    MAIL_PASSWORD = EmailCons.PASSWORD

    # mail accounts
    MAIL_DEFAULT_SENDER = EmailCons.DEFAULT_SENDER

app_config = {
    'development': Development,
    'production': Production    
}
