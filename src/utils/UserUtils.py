
from src.models.UserModel import UserModel
from src.constant import *
from src.utils.AuthUtils import AuthUtils


class UserUtils():
    @staticmethod
    def get_userid_from_token(token):
        username = AuthUtils.get_username_from_token(token)
        user = UserModel.get_user_by_username(username)
        return user.user_id

    @staticmethod
    def is_admin(token):
        username = AuthUtils.get_username_from_token(token)
        user = UserModel.get_user_by_username(username)
        return user.is_sa