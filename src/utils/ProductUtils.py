import re

def remove_vocal(s):
    result = re.sub(r'[AEIOU]', '', s, flags=re.IGNORECASE)
    return result