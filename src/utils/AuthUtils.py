import jwt
import datetime
from flask import json, Response, request, g
from functools import wraps
from ..models.UserModel import UserModel
from ..constant import *


class AuthUtils():    
    @staticmethod
    def generate_token(user_id):
        """
        Generate Token Method
        """
        try:
            payload = {
                'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=600),
                'iat': datetime.datetime.utcnow(),
                'sub': user_id
            }               
            return jwt.encode(
                payload,
                AppJwt.SECRET_KEY,
                'HS256'
            ).decode('utf-8')
        except Exception as e:                        
            return Response(
                mimetype="application/json",
                response=json.dumps({'error': 'error in generating user token'}),
                status=400
            )

    @staticmethod
    def decode_token(token):        
        re = {'data': {}, 'error': {}}
        try:
            payload = jwt.decode(token, AppJwt.SECRET_KEY)
            re['data'] = {'user_id': payload['sub']}
            return re
        except jwt.ExpiredSignatureError as e1:
            re['error'] = {'message': 'token expired, please login again'}
            return re
        except jwt.InvalidTokenError:
            re['error'] = {'message': 'Invalid token, please try again with a new token'}
            return re
        except Exception as e:
            re['error'] = {'message': 'token expired, please login again'}
            re['data'] = {'user_id': e.args[0].split()[-1]}
            return re

    @staticmethod
    def get_username_from_token(token):
        data = AuthUtils.decode_token(token)
        if data['error']: return False
        return data['data']['user_id']        

    @staticmethod
    def is_valid_jwt():
        if ('api_token' in request.headers):
            token = request.headers.get('api_token')
            data = AuthUtils.decode_token(token)
            if data['error']:return False
            user_id = data['data']['user_id']            
            check_user = UserModel.get_user_by_username(user_id)

            if not check_user: return False
            return True        
        else:
            return False

    # decorator
    @staticmethod
    def auth_required(func):        
        @wraps(func)
        def decorated_auth(*args, **kwargs):
            if 'api_token' in request.headers:
                token = request.headers.get('api_token')                                
                data = AuthUtils.decode_token(token)                
                if data['error']:
                    if data['error']['message'] == 'token expired, please login again':
                        try :
                            req_data = data['data']
                            user = UserModel.get_user(req_data['user_id'])
                            user.disconnect()
                        except KeyError:
                            pass
                        return Response(
                            mimetype="application/json",
                            response=json.dumps({"error": 'token expired, please login again'}),
                            status=400
                        )
                try:
                    user_id = data['data']['user_id']
                    check_user = UserModel.get_user_by_username(user_id)
                except KeyError:
                    return Response(
                            mimetype="application/json",
                            response=json.dumps({"error": 'token expired, please login again'}),
                            status=400
                        )
                if not check_user:
                    return Response(
                        mimetype="application/json",
                        response=json.dumps({'error': 'user does not exist, invalid token'}),
                        status=400
                    )
            else:
                return Response(
                    mimetype="application/json",
                    response=json.dumps({'error': 'Authentication token is not available, please login to get one'}),
                    status=400
                )
            return func(*args, **kwargs)

        return decorated_auth
