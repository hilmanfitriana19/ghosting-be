from src.app import mail
from flask import url_for,render_template
from itsdangerous import URLSafeTimedSerializer
from src.constant import *
from flask_mail import *
from src.constant import *

confirm_serializer = URLSafeTimedSerializer(EmailCons.SECRET_KEY)        

def send_confirmation_email(user_email,username):
    token=confirm_serializer.dumps(user_email, salt=EmailCons.SECURITY_PASSWORD_SALT)
    const = str('http://www.ghosting-forlife.com/auth/validate/'+token)
    html = render_template('email_confirmation.html',confirm_url=const,user=username)
 
    send_email(user_email,'Confirm Your Email Address', html)

def send_email(to, subject, template):
    msg = Message(
        subject,
        recipients=[to],
        html=template,
        sender=(' Jam Import Hade','hilman191025@gmail.com')
    )
    mail.send(msg)

def confirm_email(token):
    email=None
    try:
        email = confirm_serializer.loads(token, salt=EmailCons.SECURITY_PASSWORD_SALT, max_age=86400)        
    except:
        return False    
        
    return email

def send_forget_password(user_email):            
    token=confirm_serializer.dumps(user_email, salt=EmailCons.SECURITY_PASSWORD_SALT)
    const = str('http://www.ghosting-forlife.com/auth/account/'+token)
    html = render_template('forget_pass.html',change_password_url=const)
 
    send_email(user_email,'Change Password Request', html)