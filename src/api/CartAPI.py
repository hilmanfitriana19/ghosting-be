from flask import request, json, Response, Blueprint
from sqlalchemy.exc import *
from src.models.CartModel import CartModel,CartScheme
from src.utils.Response import *
from src.constant import *
from src.utils.AuthUtils import AuthUtils
from src.utils.UserUtils import UserUtils
from src.models.UserModel import UserScheme, UserModel
from src.models.ProductModel import ProductModel
from src.models.CartProductModel import CartProductModel, CartProductSchemeAdd, CartProductScheme

cart_api = Blueprint('cart_api', __name__)
cartSchema = CartScheme()
cartProductSchema = CartProductScheme()
cartProductSchemaAdd = CartProductSchemeAdd()

from ..app import limiter

@cart_api.route('/',methods=['GET'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def get_userCart():
    user_id = UserUtils.get_userid_from_token(request.headers.get('api_token'))
    userCart = CartModel.getUserCart(user_id)           
    if not userCart:
        return custom_response({"error": MessageDB.ERROR_EMPTY}, 400)    
    serialize_userCart = cartSchema.dump(userCart,many=True)        
    return custom_response({"data":serialize_userCart}, 200)

@cart_api.route('/count',methods=['GET'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def get_userCartCount():
    user_id = UserUtils.get_userid_from_token(request.headers.get('api_token'))
    userCart = CartModel.getUserCart(user_id)
    if not userCart:
        return custom_response({"error": MessageDB.ERROR_EMPTY}, 400)    
    return custom_response({"count": len(userCart[0].cart_product)}, 200)    

@cart_api.route('/product',methods=['POST'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def add_product_to_chart():
    user_id = UserUtils.get_userid_from_token(request.headers.get('api_token'))
    cart = CartModel.get_cart_by_user_id(user_id)
    if not cart:
        try:
            cart = CartModel({'cart_id':user_id,'user_id':user_id})
            cart.save()
        except:
            return custom_response({"error": MessageOperation.UNSUCCESFULL_CREATED}, 400)
    request_data = request.get_json()        
    request_data['cart_id']=cart.cart_id
    product = ProductModel.get_product_by_id(request_data.get('product_id'))
    cart.set_total(cart.total_payment+(product.price*request_data.get('quantity')))
    # adding the same product, add the quantity to the product    
    data_in_db = CartProductModel.get_card_product_by_cart_product_detailpro(request_data.get('cart_id'),request_data.get('product_id'),request_data.get('detail_product_id'))
    if data_in_db:        
        data_in_db.add_quantity(request_data.get('quantity'))
        return custom_response({"message": MessageOperation.SUCCESFULL_CREATED}, 200)
    try:                    
        data = cartProductSchemaAdd.load(request_data,partial=True)
    except:
        return custom_response({"error": MessageDB.ERROR_ARGUMENT}, 400)            
    try:        
        #create new data                    
        cartProduct = CartProductModel(data)        
        cartProduct.save()        
        return custom_response({"message": MessageOperation.SUCCESFULL_CREATED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_CREATED}, 400)

@cart_api.route('/product',methods=['DELETE'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def remove_product_from_cart():   
    request_data = request.get_json()                
    data = CartProductModel.get_card_by_id(request_data.get('cart_product_id'))
    if not data:        
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 400)    
    try:        
        #create new data        
        data.delete()
        return custom_response({"message": MessageOperation.SUCCESFULL_DELETED}, 200)        
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_DELETED}, 400)

@cart_api.route('/product',methods=['PUT'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def update_product_from_cart():
    user_id = UserUtils.get_userid_from_token(request.headers.get('api_token'))    
    cart = CartModel.get_cart_by_user_id(user_id)    
    
    request_data = request.get_json()            
    data_by_id = CartProductModel.get_card_by_id(request_data.get('cart_product_id'))
    product = ProductModel.get_product_by_id(data_by_id.product_id)    
    new_total=(request_data.get('quantity')-data_by_id.quantity)*product.price
    cart.set_total(cart.total_payment+new_total)    
    try:                    
        data_by_id.update(request_data)
        return custom_response({"message": MessageOperation.SUCCESFULL_UPDATED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_UPDATED}, 400)