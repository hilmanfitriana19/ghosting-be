from flask import request, json, Response, Blueprint
from sqlalchemy.exc import *
from src.utils.Response import *
from src.constant import *
from src.utils.AuthUtils import AuthUtils
from src.utils.UserUtils import UserUtils
from src.models.UserAddressModel import UserAddressScheme, UserAddressModel

user_address_api = Blueprint('user_address_api', __name__)
userAddressSchema = UserAddressScheme()

from ..app import limiter

@user_address_api.route('/',methods=['GET'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def get_list_user_address():
    user_id = UserUtils.get_userid_from_token(request.headers.get('api_token'))
    user_address = UserAddressModel.get_user_address(user_id)

    if not user_address:
        return custom_response({"error": MessageDB.ERROR_EMPTY}, 400)    
    serialize_user_address = userAddressSchema.dump(user_address,many=True)        
    return custom_response({"data":serialize_user_address}, 200)

@user_address_api.route('/<string:address_id>',methods=['GET'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def get_user_address(address_id):
    user_address = UserAddressModel.get_user_address_by_address_id(address_id)

    if not user_address:
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 404)    
    serialize_user_address = userAddressSchema.dump(user_address)
    return custom_response({"data":serialize_user_address}, 200)

@user_address_api.route('/',methods=['POST'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def add_user_address():
    user_id = UserUtils.get_userid_from_token(request.headers.get('api_token'))    
    request_data = request.get_json()            
    try:                    
        data = userAddressSchema.load(request_data,partial=True)
    except:
        return custom_response({"error": MessageDB.ERROR_ARGUMENT}, 400)
    data['user_id']=user_id    
    try:                    
        userAddress = UserAddressModel(data)        
        userAddress.save()    
        return custom_response({"message": MessageOperation.SUCCESFULL_CREATED,"data":{'id':userAddress.address_id}}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_CREATED}, 400)

@user_address_api.route('/<string:address_id>',methods=['DELETE'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def remove_user_address(address_id):       
    
    data = UserAddressModel.get_user_address_by_address_id(address_id)
    if not data:        
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 404)
    try:        
        #create new data        
        data.delete()
        return custom_response({"message": MessageOperation.SUCCESFULL_DELETED}, 200)        
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_DELETED}, 400)

@user_address_api.route('/',methods=['PUT'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def update_user_address():    
    
    request_data = request.get_json()
    if not request_data.get('address_id'):
        return custom_response({"error": MessageDB.ERROR_INTEGRITY}, 400)
    data_by_id = UserAddressModel.get_user_address_by_address_id(request_data.get('address_id'))    
    try:                    
        data_by_id.update(request_data)
        return custom_response({"message": MessageOperation.SUCCESFULL_UPDATED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_UPDATED}, 400)

