from flask import request, json, Response, Blueprint
from sqlalchemy.exc import *
from src.models import ProductModel,ProductScheme, ProductSchemeFull
from src.models import CategoryModel,CategoryScheme
from src.models import VariationModel, VariationScheme, VariationSchemeFull
from src.models import DetailProductModel, DetailProductScheme
from src.utils.Response import *
from src.constant import *
from src.utils.AuthUtils import AuthUtils
from src.utils.UserUtils import UserUtils
import os

product_api = Blueprint('product_api', __name__)
productSchema = ProductScheme()
productSchemaFull = ProductSchemeFull()
variationSchema = VariationScheme()
variationSchemaFull = VariationSchemeFull()
detailproductSchema = DetailProductScheme()

from ..app import limiter

@product_api.route('/', methods=['GET'])
def get_list_product_category():
    request_data = request.get_json()
    category_id = request_data.get('category')    
    if category_id:
        category = CategoryModel.get_category_by_id(category_id)
        if not category:
            return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 404)    

        listProduct = ProductModel.get_all_product_filter_by_category(category_id)
    else:
        listProduct = ProductModel.get_product()
    if not listProduct:
        return custom_response({"error": MessageDB.ERROR_EMPTY}, 404)    
    serialize_product = productSchema.dump(listProduct,many=True)
    return custom_response({"data": serialize_product}, 200)

@product_api.route('/detail/<string:product_id>', methods=['GET'])
def get_detail_product(product_id):

    product = ProductModel.get_product_full_detail_by_id(product_id)
    if not product:
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 400)    
    serialize_product = productSchemaFull.dump(product)
    return custom_response({"data": serialize_product}, 200)


@product_api.route('/', methods=['POST'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def add_new_product():
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)    
    request_data = request.get_json()    
    data = productSchema.load(request_data)
    if not data:
        return custom_response({"error": MessageDB.ERROR_INTEGRITY}, 400)    
    try:
        product = ProductModel(data)
        product.save()    
        path = FrontEnd.PATH+"/src/assets/img/product/"+product.product_id
        os.mkdir(path)
        os.system("chown -R "+User.Name+':'+User.Name+" "+path)
        return custom_response({"message": MessageOperation.SUCCESFULL_CREATED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_CREATED}, 400)
    
@product_api.route('/<string:product_id>', methods=['PUT'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def edit_product(product_id):   
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)    
    request_data = request.get_json()    
    data = productSchema.load(request_data)
    if not data:
        return custom_response({"error": MessageDB.ERROR_INTEGRITY}, 400)    

    product = ProductModel.get_product_by_id(product_id)    
    try:
        product.update(data)
        return custom_response({"message": MessageOperation.SUCCESFULL_UPDATED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_UPDATED}, 400)

@product_api.route('/variant', methods=['POST'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def add_variant():
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)    
    request_data = request.get_json()    
    if not request_data.get('variation_name'):
        return custom_response({"error": MessageDB.ERROR_INTEGRITY}, 400)
    
    variation = VariationModel({'product_id':request_data.get('product_id'),'variation_name':request_data.get('variation_name')})

    try:
        variation.save()        
        return custom_response({"message": MessageOperation.SUCCESFULL_CREATED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_CREATED}, 400)

@product_api.route('/variant', methods=['PUT'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def edit_variant():
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)    
    request_data = request.get_json()
    data = variationSchema.load(request_data)    
    if not data:
        return custom_response({"error": MessageDB.ERROR_INTEGRITY}, 400)        
    variant = VariationModel.get_variation_from_id(request_data.get('variation_id'))    
    try:
        variant.update(data)
        return custom_response({"message": MessageOperation.SUCCESFULL_UPDATED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_UPDATED}, 400)

@product_api.route('/variant', methods=['GET'])
def get_variant():    
    request_data = request.get_json()    
    if not request_data.get('variation_id'):
        return custom_response({"error": MessageDB.ERROR_INTEGRITY}, 400)        
    variant = VariationModel.get_variant_by_id_product(request_data.get('variation_id'),request_data.get('product_id'))
    if not variant:    
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 400)    
    serialize_variant = variationSchemaFull.dump(variant,many=True)
    return custom_response({"data": serialize_variant}, 200)    

@product_api.route('/detail', methods=['POST'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def add_detail():
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)    
    request_data = request.get_json()    
    data = detailproductSchema.load(request_data)
    if not data:
        return custom_response({"error": MessageDB.ERROR_INTEGRITY}, 400)    
    
    try:
        detailProduct = DetailProductModel(data)
        detailProduct.save()
        return custom_response({"message": MessageOperation.SUCCESFULL_CREATED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_CREATED}, 400)


@product_api.route('/detail', methods=['PUT'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def edit_detail():
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)    
    request_data = request.get_json()    
    data = detailproductSchema.load(request_data)    
    if not data:
        return custom_response({"error": MessageDB.ERROR_INTEGRITY}, 400)            
    detail = DetailProductModel.get_by_variation_and_detail_product(data.get('variation_id'),data.get('detail_product_id'))    
    try:
        detail.update(data)
        return custom_response({"message": MessageOperation.SUCCESFULL_UPDATED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_UPDATED}, 400)

@product_api.route('/detail', methods=['DELETE'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def remove_detail():
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)    
    request_data = request.get_json()

    detail = DetailProductModel.get_by_variation_and_detail_product(request_data.get('variation_id'),request_data.get('detail_product_id'))
    if not detail:
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 400)        
    try:
        detail.delete()
        return custom_response({"message": MessageOperation.SUCCESFULL_DELETED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_DELETED}, 400)

@product_api.route('/variant', methods=['DELETE'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def delete_variant():
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)    
    request_data = request.get_json()        
    variant = VariationModel.get_variation_from_id(request_data.get('variation_id'))    
    if not variant:
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 400)        
    try:
        variant.delete()
        return custom_response({"message": MessageOperation.SUCCESFULL_DELETED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_DELETED}, 400)

@product_api.route('/', methods=['DELETE'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def delete_product():
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)    
    request_data = request.get_json()        
    product = ProductModel.get_product_by_id(request_data.get('product_id'))
            
    if not product:
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 400)        
    try:
        product.delete()
        return custom_response({"message": MessageOperation.SUCCESFULL_DELETED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_DELETED}, 400)