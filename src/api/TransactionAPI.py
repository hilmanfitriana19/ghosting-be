from flask import request, json, Response, Blueprint
from sqlalchemy.exc import *
from src.utils.Response import *
from src.constant import *
from src.utils.AuthUtils import AuthUtils
from src.utils.UserUtils import UserUtils
from src.models.TransactionModel import TransactionModel, TransactionScheme
from src.models.PaymentModel import PaymentModel, PaymentScheme
from src.models.CartProductModel import CartProductModel
from src.models.CartModel import CartDetailProductScheme, CartModel
from src.models.TransactionProductModel import TransactionProductModel
from src.models.ProductModel import ProductModel
from src.models.DetailProductModel import DetailProductModel

transaction_api = Blueprint('transaction_api', __name__)
transactionSchema = TransactionScheme()
paymentSchema = PaymentScheme()
cartDetailProductSchema = CartDetailProductScheme()

from ..app import limiter

@transaction_api.route('/', methods=['GET'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def get_list_transaction():
    user_id = UserUtils.get_userid_from_token(request.headers.get('api_token'))
    request_data = request.get_json()            
    transaction = TransactionModel.get_transaction_by_user(user_id)
    if not transaction:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_GET}, 400)
                
    serialize_transaction = transactionSchema.dump(transaction,many=True)    
    listData = []
    for data in serialize_transaction:
        element = {}        
        element['total'] = data['total_payment']
        element['date'] = data['date']
        element['product'] = []
        for listTransaction in data['transaction_product']:
            listElement = {}
            detail = TransactionProductModel.get_transaction_by_detail_product_id(listTransaction['detail_product_id'])
            product = ProductModel.get_product_by_id(detail.product_id)
            listElement['product_name'] = product.name
            listElement['detail_name'] = listTransaction['detail_product']
            listElement['quantity'] = listTransaction['quantity']
            element['product'].append(listElement)
        
        listData.append(element)

    return custom_response({"data": listData}, 200)

@transaction_api.route('/', methods=['POST'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def create_transaction():
    user_id = UserUtils.get_userid_from_token(request.headers.get('api_token'))
    request_data = request.get_json()
    transaction = TransactionModel({'total_payment':request_data.get('total_payment'),'user_address_id':request_data.get('address_id'),'user_id':user_id})    
    try:
        transaction.save()
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_CREATED}, 400)    
    try:
        cart_product=CartModel.get_cart_by_user_id(user_id)

        detailCard = cartDetailProductSchema.dump(cart_product)
        for data in detailCard['cart_product']:        
            transaction_product = TransactionProductModel({'transaction_id':transaction.transaction_id,'detail_product_id':data.get('detail_product_id'),'quantity':data.get('quantity'),'product_id':data.get('product_id')})
            detail_product = DetailProductModel.get_detail_by_id(data.get('detail_product_id'))
            detail_product.update_stock(detail_product.stock-data.get('quantity'))        
            transaction_product.save()
            cart_product = CartProductModel.get_card_by_id(data.get('cart_product_id'))
            cart_product.delete()
        cart_product.set_total(0)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_CREATED}, 400)
    
    try:
        payment = PaymentModel({'card_number':request_data.get('card_number'),
                                'total_payment':request_data.get('total_payment'),
                                'name':request_data.get('name'),
                               'method':request_data.get('method'),
                                'cvc':request_data.get('cvc'),
                                'expiration_month':request_data.get('expiration_month'),
                                'expiration_year':request_data.get('expiration_year'),
                                })    
    
        return custom_response({"message": MessageOperation.SUCCESFULL_CREATED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_CREATED}, 400)
    