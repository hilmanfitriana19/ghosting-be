from flask import request, json, Response, Blueprint
from sqlalchemy.exc import *
from src.models import UserModel, UserScheme, UserLoginScheme
from src.utils import *
from src.Email import *
from src.utils.Response import *
from src.constant import *
from src.utils.AuthUtils import AuthUtils
from src.models.CartModel import CartModel
from src.utils.UserUtils import UserUtils

auth_user_api = Blueprint('auth_user_api', __name__)
userSchema = UserScheme()
userLoginSchema = UserLoginScheme()

from ..app import limiter
import datetime

@auth_user_api.route('/register', methods=['POST'])
def register():
    request_data = request.get_json()
    try:
        data = userSchema.load(request_data)
    except :
        return custom_response({"error": MessageDB.ERROR_ARGUMENT}, 400)
    
    if UserModel.get_user_by_username(data.get('username')):
        message = {"error": str("Username "+data.get('username')+" Already Exist")}
        return custom_response(message, 400)

    if UserModel.get_user_by_email(data.get('email')):
        message = {"error": str("Email "+data.get('email')+" Already Used")}
        return custom_response(message, 400)        

    if UserModel.get_user_by_phone(data.get('phone')):
        message = {"error": str("Email "+data.get('email')+" Already Used")}
        return custom_response(message, 400)        

    try:        
        user = UserModel(data)
        user.save()
        cart = CartModel({'user_id':user.user_id})
        cart.save()
        # send_confirmation_email(user.email,user.username)
        return custom_response({"message": MessageOperation.SUCCESFULL_CREATED}, 200)    
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_CREATED}, 400)


@auth_user_api.route('/verify/<string:token>')
def verify_email(token):        
    email = confirm_email(token)
    if not email:    
        return custom_response({"error": "Verify Email Failed"}, 400)
    
    user = UserModel.get_user_by_email(email)
    if not user:    
        return custom_response({"error":"User Not Registered"}, 404)    
    try:
        if user.check_lifetime_token():
            user.verify_user_mail()
            return custom_response({"message": "Email Successfully Verified"}, 200)
        else:
            return custom_response({"error": "Token Expired"}, 200)
    except:
        return custom_response({"error": "Verify Email Failed"}, 400)

@auth_user_api.route('/forget', methods=['POST'])
def forget():
    request_data = request.get_json()    
    
    user = UserModel.get_user_by_email(request_data.get('email'))
    if not user:
        message = {"error": str("Email "+request_data.get('email')+" Not Registered In Our Apps")}
        return custom_response(message, 400)        

    try:        
        send_forget_password(user.email)                        
        return custom_response({"message": "Request Change Password Successfully"}, 200)    
    except:
        return custom_response({"error": "Failed to Request Change Password"}, 400)

@auth_user_api.route('/forget/<string:token>',methods=['POST'])
def change_password(token):
    email = confirm_email(token)
    if not email:    
        return custom_response({"error": "Token Expired"}, 400)    
    user = UserModel.get_user_by_email(email)
    if not user:    
        return custom_response({"error":"User Not Registered"}, 404)    
    
    request_data = request.get_json()    
    try:        
        user.update_pass(request_data.get('password'))
        return custom_response({"message": "Password Has Been Changed"}, 200)        
    except:
        return custom_response({"error": "Failed To Change Password"}, 400)

def check():
    req_data = request.get_json()

    data = userLoginSchema.load(req_data,partial=True)
    
    if not data.get('username') or not data.get('password'):
        return False
    user = UserModel.get_user_by_username(data.get('username'))
    if not user:
        return False
    if not user.check_hash(data.get('password')):
        return False        
    # if not user.email_confirmed():
    #     return False

    return True

@auth_user_api.route('/logout', methods=['POST'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def logout():    
    try:
        token = request.headers.get('api_token')        
        data = AuthUtils.decode_token(token)
        user_id = data['data']['user_id']        
        user = UserModel.get_user_by_username(user_id)
        return custom_response({"message": 'Successfully Logout'}, 200)
    except:
        return custom_response({"error": 'Unsuccessfully Logout'}, 400)


@auth_user_api.route('/', methods=['POST'])
@limiter.limit(LimiterAPI.AUTH_LIMITER, exempt_when=lambda: check())
def login():    
    req_data = request.get_json()
    try:
        data= userLoginSchema.load(req_data,partial=True)
    except:
        return custom_response({'error': 'you need username and password to sign in'}, 400)    
    
    if not data.get('username') or not data.get('password'):
        return custom_response({'error': 'you need username and password to sign in'}, 400)    
    user = UserModel.get_user_by_username(data.get('username'))        
    if not user:
        if len(data.get('username').split('@'))==2:
            user = UserModel.get_user_by_email(data.get('username'))
            
    if not user:
        return custom_response({'error': 'Wrong Username/Email or Password'}, 400)
    if not user.check_hash(data.get('password')):
        return custom_response({'error': 'Wrong Username/Email or Password'}, 400)    
    if not user.email_confirmed():
        return custom_response({'error': 'Email Has Not Validate'}, 400)    
    token = AuthUtils.generate_token(user.username)    
    return custom_response({"token": str(token),'username':user.username}, 200)

@auth_user_api.route('/',methods=['GET'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def get_userData():
    user_id = UserUtils.get_userid_from_token(request.headers.get('api_token'))    
    user = UserModel.get_user(user_id)
    if not user:
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 404)
    serialize_user = userSchema.dump(user)
    return custom_response({"data":serialize_user}, 200)

@auth_user_api.route('/', methods=['PUT'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def edit_user():   
    user_id = UserUtils.get_userid_from_token(request.headers.get('api_token'))    
    user = UserModel.get_user(user_id)    
    if not user:
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 404)

    request_data = request.get_json()        
    login_again=False
    if user.username != request_data.get('username'):
        login_again=True
    try:
        user.update(request_data)                                
        if login_again:
            token = AuthUtils.generate_token(request_data.get('username'))            
            return custom_response({"message": MessageOperation.SUCCESFULL_UPDATED,"data":{"token":token,"username":user.username}}, 200)

        return custom_response({"message": MessageOperation.SUCCESFULL_UPDATED}, 200)
    except:
        return custom_response({"error": MessageOperation.UNSUCCESFULL_UPDATED}, 400)
