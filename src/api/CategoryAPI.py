from flask import request, json, Response, Blueprint, send_file
from sqlalchemy.exc import *
from src.models import CategoryModel,CategoryScheme, CategoryProductScheme
from src.utils.Response import *
from src.utils.UserUtils import UserUtils
from src.constant import *
from src.utils.AuthUtils import AuthUtils

category_api = Blueprint('category_api', __name__)
categorySchema = CategoryScheme()
categoryProductSchema = CategoryProductScheme()

from ..app import limiter

@category_api.route('/', methods=['GET'])
def get_list_category():
    listcategory = CategoryModel.get_category()                
    if not listcategory:
        return custom_response({"error": MessageDB.ERROR_EMPTY}, 404)        
    serialize_category = categorySchema.dump(listcategory,many=True)    
    return custom_response({"data": serialize_category}, 200)

@category_api.route('/detail', methods=['GET'])
def get_list_category_detail():
    listcategory = CategoryModel.get_category()                
    if not listcategory:
        return custom_response({"error": MessageDB.ERROR_EMPTY}, 404)            
    serialize_category = categoryProductSchema.dump(listcategory,many=True)
    return custom_response({"data": serialize_category}, 200)

@category_api.route('/', methods=['POST'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def create_category():
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)
    request_data = request.get_json()
    data = categorySchema.load(request_data)
    if not data:
        return custom_response({"error": MessageDB.ERROR_INTEGRITY}, 400)
    
    try:
        category = CategoryModel(data)
        category.save()
        return custom_response({"message":MessageOperation.SUCCESFULL_CREATED}, 200)
    except:    
        return custom_response({"error":MessageOperation.UNSUCCESFULL_CREATED}, 400)
    
@category_api.route('/<int:id>', methods=['DELETE'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def delete_category(id):
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)
    data = CategoryModel.get_category_by_id(id)
    if not data:
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 404)
    try:        
        data.delete()
        return custom_response({"message":MessageOperation.SUCCESFULL_DELETED}, 200)
    except:    
        return custom_response({"error":MessageOperation.UNSUCCESFULL_DELETED}, 400)

@category_api.route('/<int:id>', methods=['PUT'])
@limiter.limit(LimiterAPI.TOKEN_LIMITER, exempt_when=lambda: AuthUtils.is_valid_jwt())
@AuthUtils.auth_required
def edit_category(id):        
    if not UserUtils.is_admin(request.headers.get('api_token')):
        return custom_response({"error": MessageOperation.FAILED_OPERATION}, 400)
    request_data = request.get_json()
    data = categorySchema.load(request_data)
    if not data:
        return custom_response({"error": MessageDB.ERROR_NOTFOUND}, 404)
    
    category = CategoryModel.get_category_by_id(id)
    try:        
        category.update(data)
        return custom_response({"message":MessageOperation.SUCCESFULL_UPDATED}, 200)
    except:    
        return custom_response({"error":MessageOperation.UNSUCCESFULL_UPDATED}, 400)
    
    