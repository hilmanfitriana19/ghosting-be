from flask import *
from flask_mail import *
from flask_cors import CORS, cross_origin
from .config import app_config
from .models import db,bcrypt
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from src.constant import *

limiter = None
mail = None
app = None

def create_app(env_name):    
  global app
  app = Flask(__name__)
  global mail  
  app.config.from_object(app_config[env_name])
  mail = Mail(app)
    
  bcrypt.init_app(app)
  db.init_app(app)
  
  global limiter
  limiter = Limiter(
    app,
    key_func=get_remote_address    
  )
  
  from .api.AuthAPI import auth_user_api as auth_blueprint
  from .api.ProductAPI import product_api as product_blueprint
  from .api.CartAPI import cart_api as cart_blueprint
  from .api.CategoryAPI import category_api as category_blueprint
  from .api.UserAddressAPI import user_address_api as address_blueprint
  from .api.TransactionAPI import transaction_api as transaction_blueprint

  app.register_blueprint(auth_blueprint, url_prefix=UrlAPI.AUTH_API)
  app.register_blueprint(product_blueprint, url_prefix=UrlAPI.PRODUCT_API)
  app.register_blueprint(cart_blueprint, url_prefix=UrlAPI.CART_API)
  app.register_blueprint(category_blueprint, url_prefix=UrlAPI.CATEGORY_API)
  app.register_blueprint(address_blueprint, url_prefix=UrlAPI.USER_ADDRESS_API)
  app.register_blueprint(transaction_blueprint,url_prefix=UrlAPI.TRANSACTION_API)

  @app.errorhandler(429)
  def ratelimit_handler(e):
    return Response(
      mimetype="application/json",
      response=json.dumps({'error': "ratelimit exceeded %s" % e.description}),
      status=429
    )  
  cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

  return app
